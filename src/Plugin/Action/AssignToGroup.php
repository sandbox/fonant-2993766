<?php

namespace Drupal\group_vbo\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContentType;

/**
 * Assign to Group.
 *
 * @Action(
 *   id = "assign_to_group",
 *   label = @Translation("Assign nodes to a Group"),
 *   type = "node",
 *   pass_context = TRUE
 * )
 */
class AssignToGroup extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(\Drupal\node\NodeInterface $node = NULL) {
    if ($node === NULL) {
      return;
    }
    /** @var Group $group */
    $group = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->load($this->configuration['group_id']);
    // Check whether this node type is able to be added to this group.
    $plugin_id = 'group_node:' . $node->bundle();
    $group_content_types = GroupContentType::loadByContentPluginId($plugin_id);
    if (empty($group_content_types)) {
      $group_type_id = $group->getGroupType()->id();
      $messenger = \Drupal::messenger();
      $messenger->addMessage(
        $this->t('The <a href="@url">Group content plugin</a> is not installed for node type %bundle, so those nodes could not be assigned to group %group.',
          [
            '@url' => '/admin/group/types/manage/' . $group_type_id . '/content',
            '%bundle' => $node->bundle(),
            '%group' => $group->label(),
          ]),
        'warning');
      return;
    }
    // Check whether this node already belongs to the group.
    $group_contents = \Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties([
        'type' => array_keys($group_content_types),
        'entity_id' => $node->id(),
        'gid' => $this->configuration['group_id'],
      ]);
    // If not already assigned, add this node to the group.
    if (empty($group_contents)) {
      $group->addContent($node, $plugin_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [0 => ' - select -'];
    $group_types = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo('group');
    foreach ($group_types as $machine_name => $info) {
      $type_name = $info['label'];
      $values = [
        'type' => $machine_name,
      ];
      $groups = \Drupal::entityTypeManager()
        ->getListBuilder('group')
        ->getStorage()
        ->loadByProperties($values);
      foreach ($groups as $group) {
        $options[$type_name][$group->id()] = $group->label();
      }
    }
    $form['group_id'] = [
      '#title' => 'Assign these nodes to Group',
      '#type' => 'select',
      '#options' => $options,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $group_id = $form_state->getValues()['group_id'];
    if ($group_id == 0) {
      $el = $form['group_id'];
      $form_state->setError($el, $el['#title'] . ': ' . $this->t('Please select a Group.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['group_id'] = $form_state->getValue('group_id');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $object */
    $result = $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
