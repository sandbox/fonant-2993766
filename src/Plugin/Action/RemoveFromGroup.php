<?php

namespace Drupal\group_vbo\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupContentType;

/**
 * Remove from Group.
 *
 * @Action(
 *   id = "remove_from_group",
 *   label = @Translation("Remove nodes from a Group"),
 *   type = "node"
 * )
 */
class RemoveFromGroup extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(\Drupal\node\NodeInterface $node = NULL) {
    $group = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->load($this->configuration['group_id']);
    $plugin_id = 'group_node:' . $node->bundle();
    $group_content_types = GroupContentType::loadByContentPluginId($plugin_id);
    if (empty($group_content_types)) {
      return;
    }
    $group_contents = \Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties([
        'type' => array_keys($group_content_types),
        'entity_id' => $node->id(),
        'gid' => $this->configuration['group_id'],
      ]);
    foreach ($group_contents as $group_content) {
      $group_content->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $group_types = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo('group');
    foreach ($group_types as $machine_name => $info) {
      $type_name = $info['label'];
      $values = [
        'type' => $machine_name,
      ];
      $groups = \Drupal::entityTypeManager()
        ->getListBuilder('group')
        ->getStorage()
        ->loadByProperties($values);
      foreach ($groups as $group) {
        $options[$type_name][$group->id()] = $group->label();
      }
    }
    $form['group_id'] = [
      '#title' => 'Remove from Group',
      '#type' => 'select',
      '#options' => $options,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['group_id'] = $form_state->getValue('group_id');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $object */
    $result = $object->access('update', $account, TRUE);

    return $return_as_object ? $result : $result->isAllowed();
  }

}
